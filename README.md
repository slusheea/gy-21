<br />
<div align="center">
  <a href="https://crates.io/crates/gy_21">
    <img src="images/GY-21.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">GY-21</h3>

  <p align="center">
    Driver crate for the HTU21D temperature and relative humidity sensor
    <br />
    <a href="https://docs.rs/gy_21"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/slusheea/gy-21/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/slusheea/gy-21/-/issues">Request Feature</a>
  </p>
</div>

## Built with
[`embedded_hal`](https://crates.io/crates/embedded-hal)

## Getting started
Check out the [documentation](https://docs.rs/gy_21) and [examples](/examples) folder for directions on how to use the crate.

## License
The contents of this repository are dual-licensed under the MIT OR Apache 2.0 License. That means you can choose either the MIT license or the Apache-2.0 license when you re-use this code. See `LICENSE-MIT` or `LICENSE-APACHE` for more information on each specific license.

Any submissions to this project (e.g. as Pull Requests) must be made available under these terms.

## Contact
Raise an issue: https://gitlab.com/slusheea/gy-21/-/issues or find other ways to contact me on my website: https://slushee.dev/
