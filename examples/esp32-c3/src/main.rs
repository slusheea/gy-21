#![no_std]
#![no_main]

use esp_backtrace as _;
use esp_println::println;
use hal::{clock::ClockControl, i2c::I2C, peripherals::Peripherals, prelude::*, Delay, IO};

use gy_21::Gy21;

#[entry]
fn main() -> ! {
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();

    let clocks = ClockControl::max(system.clock_control).freeze();
    let mut delay = Delay::new(&clocks);

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);

    let i2c = I2C::new(
        peripherals.I2C0,
        io.pins.gpio1,
        io.pins.gpio2,
        100u32.kHz(),
        &clocks,
    );

    let mut gy_21 = Gy21::new(i2c, delay);

    loop {
        if let Ok(temp) = gy_21.temperature() {
            println!("Temperature = {}", temp);
        }
        if let Ok(rhum) = gy_21.humidity() {
            println!("Relative humidity = {}%", rhum);
        }
        if let Ok(dpt) = gy_21.dew_point_temp() {
            println!("Dew point temperature = {}\n\n", dpt);
        }
    }
}
