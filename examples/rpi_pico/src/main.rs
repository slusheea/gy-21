#![no_std]
#![no_main]

use bsp::entry;
use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},
    fugit::RateExtU32,
    gpio::{FunctionI2C, Pin, PullUp},
    pac,
    sio::Sio,
    watchdog::Watchdog,
    I2C,
};
use rp_pico as bsp;

use panic_probe as _;

use defmt::info;
use defmt_rtt as _;

use gy_21::Gy21;

#[entry]
fn main() -> ! {
    info!("Program start");
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    // External high-speed crystal on the pico board is 12Mhz
    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let sda: Pin<_, FunctionI2C, PullUp> = pins.gpio18.reconfigure();
    let scl: Pin<_, FunctionI2C, PullUp> = pins.gpio19.reconfigure();

    let i2c = I2C::i2c1(
        pac.I2C1,
        sda,
        scl,
        100.kHz(),
        &mut pac.RESETS,
        &clocks.system_clock,
    );

    let mut gy_21 = Gy21::new(i2c, delay);

    loop {
        if let Ok(temp) = gy_21.temperature() {
            info!("Temperature = {}", temp);
        }
        if let Ok(rhum) = gy_21.humidity() {
            info!("Relative humidity = {}%", rhum);
        }
        if let Ok(dpt) = gy_21.dew_point_temp() {
            info!("Dew point temperature = {}\n\n", dpt);
        }
    }
}
